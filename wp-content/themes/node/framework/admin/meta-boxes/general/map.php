<?php

$general_meta_box = node_edge_add_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post'),
        'title' => 'General',
        'name' => 'general_meta'
    )
);
	node_edge_add_meta_box_field(
		array(
			'name'        => 'edgtf_predefined_h_tags_style',
			'type'        => 'selectblank',
			'label'       => 'Predefined H tags styles',
			'description' => 'Choose predefined style',
			'parent'      => $general_meta_box,
			'default_value' => '',
			'options'     => array(
				'edgtf-h-style-1' => 'Enable'
			)
		)
	);

    node_edge_add_meta_box_field(
        array(
            'name' => 'edgtf_page_background_color_meta',
            'type' => 'color',
            'default_value' => '',
            'label' => 'Page Background Color',
            'description' => 'Choose background color for page content',
            'parent' => $general_meta_box
        )
    );
	
	$edgtf_content_padding_group = node_edge_add_admin_group(array(
		'name'        => 'content_padding_group',
		'title'       => 'Content Style',
		'description' => 'Define styles for Content area',
		'parent'      => $general_meta_box
	));
	
	$edgtf_content_padding_row = node_edge_add_admin_row(array(
		'name'   => 'edgtf_content_padding_row',
		'next'   => true,
		'parent' => $edgtf_content_padding_group
	));

	node_edge_add_meta_box_field(
		array(
			'name'          => 'edgtf_page_content_top_padding',
			'type'          => 'textsimple',
			'default_value' => '',
			'label'         => 'Content Top Padding',
			'parent'        => $edgtf_content_padding_row,
			'args'          => array(
				'suffix' => 'px'
			)
		)
	);
	node_edge_add_meta_box_field(
		array(
			'name'          => 'edgtf_page_content_right_padding',
			'type'          => 'textsimple',
			'default_value' => '',
			'label'         => 'Content Right Padding',
			'parent'        => $edgtf_content_padding_row,
			'args'          => array(
				'suffix' => 'px'
			)
		)
	);
	node_edge_add_meta_box_field(
		array(
			'name'          => 'edgtf_page_content_bottom_padding',
			'type'          => 'textsimple',
			'default_value' => '',
			'label'         => 'Content Bottom Padding',
			'parent'        => $edgtf_content_padding_row,
			'args'          => array(
				'suffix' => 'px'
			)
		)
	);
	node_edge_add_meta_box_field(
		array(
			'name'          => 'edgtf_page_content_left_padding',
			'type'          => 'textsimple',
			'default_value' => '',
			'label'         => 'Content Left Padding',
			'parent'        => $edgtf_content_padding_row,
			'args'          => array(
				'suffix' => 'px'
			)
		)
	);

	$edgtf_content_padding_row2 = node_edge_add_admin_row(array(
		'name'   => 'edgtf_content_padding_row2',
		'next'   => true,
		'parent' => $edgtf_content_padding_group
	));

	node_edge_add_meta_box_field(
		array(
			'name'    => 'edgtf_page_content_top_padding_mobile',
			'type'    => 'selectblanksimple',
			'label'   => 'Set this top padding for mobile header',
			'parent'  => $edgtf_content_padding_row2,
			'options' => array(
				'yes' => 'Yes',
				'no'  => 'No',
			)
		)
	);

	node_edge_add_meta_box_field(array(
		'name'          => 'edgtf_overlapping_content_enable_meta',
		'type'          => 'yesno',
		'default_value' => 'no',
		'label'         => 'Enable Overlapping Content',
		'description'   => 'Enabling this option will make content overlap title area',
		'parent'        => $general_meta_box
	));

    node_edge_add_meta_box_field(
        array(
            'name' => 'edgtf_page_slider_meta',
            'type' => 'text',
            'default_value' => '',
            'label' => 'Slider Shortcode',
            'description' => 'Paste your slider shortcode here',
            'parent' => $general_meta_box
        )
    );

    node_edge_add_meta_box_field(
        array(
            'name'        => 'edgtf_page_transition_type',
            'type'        => 'selectblank',
            'label'       => 'Page Transition',
            'description' => 'Choose the type of transition to this page',
            'parent'      => $general_meta_box,
            'default_value' => '',
            'options'     => array(
                'no-animation' => 'No animation',
                'fade' => 'Fade'
            )
        )
    );

    node_edge_add_meta_box_field(
        array(
            'name'        => 'edgtf_page_comments_meta',
            'type'        => 'selectblank',
            'label'       => 'Show Comments',
            'description' => 'Enabling this option will show comments on your page',
            'parent'      => $general_meta_box,
            'options'     => array(
                'yes' => 'Yes',
                'no' => 'No',
            )
        )
    );