<?php

$edgt_pages = array();
$pages = get_pages(); 
foreach($pages as $page) {
	$edgt_pages[$page->ID] = $page->post_title;
}

global $node_edge_IconCollections;

//Portfolio Images

$edgtPortfolioImages = new NodeEdgeMetaBox("portfolio-item", "Portfolio Images (multiple upload)", '', '', 'portfolio_images');
$node_edge_Framework->edgtMetaBoxes->addMetaBox("portfolio_images",$edgtPortfolioImages);

	$edgt_portfolio_image_gallery = new NodeEdgeMultipleImages("edgt_portfolio-image-gallery","Portfolio Images","Choose your portfolio images");
	$edgtPortfolioImages->addChild("edgt_portfolio-image-gallery",$edgt_portfolio_image_gallery);

//Portfolio Images/Videos 2

$edgtPortfolioImagesVideos2 = new NodeEdgeMetaBox("portfolio-item", "Portfolio Images/Videos (single upload)");
$node_edge_Framework->edgtMetaBoxes->addMetaBox("portfolio_images_videos2",$edgtPortfolioImagesVideos2);

	$edgt_portfolio_images_videos2 = new NodeEdgeImagesVideosFramework("Portfolio Images/Videos 2","ThisIsDescription");
	$edgtPortfolioImagesVideos2->addChild("edgt_portfolio_images_videos2",$edgt_portfolio_images_videos2);

//Portfolio Additional Sidebar Items

//$edgtAdditionalSidebarItems = new NodeEdgeMetaBox("portfolio-item", "Additional Portfolio Sidebar Items");
//$node_edge_Framework->edgtMetaBoxes->addMetaBox("portfolio_properties",$edgtAdditionalSidebarItems);
$edgtAdditionalSidebarItems = node_edge_add_meta_box(
    array(
        'scope' => array('portfolio-item'),
        'title' => 'Additional Portfolio Sidebar Items',
        'name' => 'portfolio_properties'
    )
);

	$edgt_portfolio_properties = node_edge_add_options_framework(
	    array(
	        'label' => 'Portfolio Properties',
	        'name' => 'edgt_portfolio_properties',
	        'parent' => $edgtAdditionalSidebarItems
	    )
	);

	//$edgt_portfolio_properties = new NodeEdgeOptionsFramework("Portfolio Properties","ThisIsDescription");
	//$edgtAdditionalSidebarItems->addChild("edgt_portfolio_properties",$edgt_portfolio_properties);

if(!function_exists('node_edge_add_attachment_custom_field')){
	function node_edge_add_attachment_custom_field( $form_fields, $post = null ) {
		if(wp_attachment_is_image($post->ID)){
			$field_value = get_post_meta( $post->ID, '_ptf_single_masonry_image_size', true );

			$form_fields['ptf_single_masonry_image_size'] = array(
				'input' => 'html',
				'label' => esc_html__( 'Image Size', 'node'),
				'helps' => esc_html__( 'Choose image size for masonry single', 'node')
			);

			$form_fields['ptf_single_masonry_image_size']['html']  = "<select name='attachments[{$post->ID}][ptf_single_masonry_image_size]'>";
			$form_fields['ptf_single_masonry_image_size']['html'] .= '<option '.selected($field_value,'edgtf-default-masonry-item',false).' value="edgtf-default-masonry-item">'.esc_html__('Default Size','node').'</option>';
			$form_fields['ptf_single_masonry_image_size']['html'] .= '<option '.selected($field_value,'edgtf-large-height-masonry-item',false).' value="edgtf-large-height-masonry-item">'.esc_html__('Large Height','node').'</option>';
			$form_fields['ptf_single_masonry_image_size']['html'] .= '<option '.selected($field_value,'edgtf-large-width-masonry-item',false).' value="edgtf-large-width-masonry-item">'.esc_html__('Large Width','node').'</option>';
			$form_fields['ptf_single_masonry_image_size']['html'] .= '<option '.selected($field_value,'edgtf-large-width-height-masonry-item',false).' value="edgtf-large-width-height-masonry-item">'.esc_html__('Large Width/Height','node').'</option>';
			$form_fields['ptf_single_masonry_image_size']['html'] .= '</select>';

		}
		return $form_fields;
	}
	add_filter( 'attachment_fields_to_edit', 'node_edge_add_attachment_custom_field' , 10, 2 );
}

if(!function_exists('node_edge_image_attachment_fields_to_save')){
	/**
	 * @param array $post
	 * @param array $attachment
	 * @return array
	 */
	function node_edge_image_attachment_fields_to_save($post, $attachment) {

		if( isset($attachment['ptf_single_masonry_image_size']) ){
			update_post_meta($post['ID'], '_ptf_single_masonry_image_size', $attachment['ptf_single_masonry_image_size']);
		}

		return $post;
	}
	add_filter( 'attachment_fields_to_save', 'node_edge_image_attachment_fields_to_save',10,2 );
}