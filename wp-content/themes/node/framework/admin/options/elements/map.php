<?php

if ( ! function_exists('node_edge_load_elements_map') ) {
	/**
	 * Add Elements option page for shortcodes
	 */
	function node_edge_load_elements_map() {

		node_edge_add_admin_page(
			array(
				'slug' => '_elements_page',
				'title' => 'Elements',
				'icon' => 'fa fa-header'
			)
		);

		do_action( 'node_edge_options_elements_map' );

	}

	add_action('node_edge_options_map', 'node_edge_load_elements_map', 11);

}