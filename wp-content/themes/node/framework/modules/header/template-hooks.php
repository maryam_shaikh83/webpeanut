<?php

//top header bar
add_action('node_edge_before_page_header', 'node_edge_get_header_top');

//mobile header
add_action('node_edge_after_page_header', 'node_edge_get_mobile_header');