<?php

if (!function_exists('node_edge_register_widgets')) {

	function node_edge_register_widgets() {

		$widgets = array(
			'NodeEdgeFullScreenMenuOpener',
			'NodeEdgeLatestPosts',
			'NodeEdgeSearchOpener',
			'NodeEdgeSideAreaOpener',
			'NodeEdgeStickySidebar',
			'NodeEdgeSocialIconWidget',
			'NodeEdgeSeparatorWidget'
		);

		foreach ($widgets as $widget) {
			register_widget($widget);
		}
	}
}

add_action('widgets_init', 'node_edge_register_widgets');