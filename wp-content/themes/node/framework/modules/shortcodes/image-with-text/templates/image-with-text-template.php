<div class="edgtf-image-with-text">
	<?php if($link) : ?>
		<a class="edgtf-iwt-link" href="<?php echo esc_url($link); ?>" <?php node_edge_inline_attr($target, 'target'); ?>></a>
	<?php endif; ?>
	<div class="edgtf-iwt-image">
			<img src="<?php echo esc_url($image); ?>" alt="" />
	</div>
	<div class="edgtf-iwt-text-holder">
		<div class="edgtf-iwt-text-table">
			<div class="edgtf-iwt-text-cell">
				<?php if($title): ?>
					<h6 class="edgtf-iwt-title">
						<?php if($link) : ?>
							<a href="<?php echo esc_url($link); ?>" <?php node_edge_inline_attr($target, 'target'); ?>>
								<?php echo esc_attr($title); ?>
							</a>
						<?php else: ?>
							<?php echo esc_attr($title); ?>
						<?php endif; ?>
					</h6>
				<?php endif; ?>
				<?php echo node_edge_get_separator_html(array('position' => 'center','thickness' => '2')); ?>
			</div>
		</div>
	</div>
</div>