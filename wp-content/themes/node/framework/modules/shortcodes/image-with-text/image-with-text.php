<?php
namespace NodeEdge\Modules\ImageWithText;

use NodeEdge\Modules\Shortcodes\Lib\ShortcodeInterface;

class ImageWithText implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'edgtf_image_with_text';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => 'Image With Text',
					'base' => $this->base,
					'category' => 'by EDGE',
					'icon' => 'icon-wpb-image-with-text extended-custom-icon',
					'params' => array(
						array(
							'type' => 'attach_image',
							'class' => '',
							'heading' => 'Image',
							'param_name' => 'image',
							'value' => '',
							'description' => ''
						),
						array(
							'type' => 'textfield',
							'admin_label' => true,
							'heading' => 'Title',
							'param_name' => 'title',
							'value' => '',
							'description' => ''
						),
						array(
							'type' => 'textfield',
							'heading' => 'Link',
							'param_name' => 'link',
							'value' => '',
							'description' => ''
						),
						array(
							'type'        => 'dropdown',
							'heading'     => 'Link Target',
							'param_name'  => 'target',
							'value'       => array(
								'Self'  => '_self',
								'Blank' => '_blank'
							),
							'dependency'  => array('element' => 'link', 'not_empty' => true)
						),
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'image'     => '',
			'title'     => '',
			'link'     	=> '',
			'target'    => '_self'
		);
		
		$params = 	shortcode_atts($args, $atts);
		extract($params);

		$params['image']= wp_get_attachment_url($params['image']); ;

		$html = node_edge_get_shortcode_module_template_part('templates/image-with-text-template', 'image-with-text', '', $params);

		return $html;
	}

}
