<?php

if(!function_exists('node_edge_get_separator_html')) {
    /**
     * Calls separator shortcode with given parameters and returns it's output
     * @param $params
     *
     * @return mixed|string
     */
    function node_edge_get_separator_html($params = array()) {
        $separator_html = node_edge_execute_shortcode('edgtf_separator', $params);
		$separator_html = str_replace("\n", '', $separator_html);
        return $separator_html;
    }
}