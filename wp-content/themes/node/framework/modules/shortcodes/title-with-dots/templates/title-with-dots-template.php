<?php
/**
 * Title With Dots shortcode template
 */
?>
<<?php echo esc_attr($title_tag);?> class="edgtf-title-with-dots" <?php node_edge_inline_style($title_style); ?>>
	<?php echo esc_html($title);?>
</<?php echo esc_attr($title_tag);?>>