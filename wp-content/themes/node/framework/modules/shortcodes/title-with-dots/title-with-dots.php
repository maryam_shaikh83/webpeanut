<?php
namespace NodeEdge\Modules\Shortcodes\TitleWithDots;

use NodeEdge\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class TitleWithDots
 */
class TitleWithDots implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'edgtf_title_with_dots';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see edgt_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		vc_map( array(
				'name' => esc_html__('Edge Title With Dots', 'node'),
				'base' => $this->getBase(),
				'category' => 'by EDGE',
				'icon' => 'icon-wpb-title-with-dots extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => 'Title',
						'param_name' => 'title',
						'value' => '',
						'admin_label' => true
					),
					array(
						"type" => "dropdown",
						"heading" => "Title Tag",
						"param_name" => "title_tag",
						"value" => array(
							"h2" => "h2",
							"h3" => "h3",
							"h4" => "h4",
							"h5" => "h5",
							"h6" => "h6"
						),
						'dependency' => Array('element' => 'title', 'not_empty' => true)
					),
					array(
						'type' => 'colorpicker',
						'heading' => 'Title Color',
						'param_name' => 'title_color',
						'description' => '',
						'dependency' => Array('element' => 'title', 'not_empty' => true)
					)

				)
		) );
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'title_tag' => 'h2',
			'title_color' => '',
			'title' => ''
		);

		$params = shortcode_atts($args, $atts);

		$params['title_style'] = '';

		if(!empty($params['title_color'])) {
			$params['title_style'] = 'color:' . $params['title_color'];
		}

		//Get HTML from template
		$html = node_edge_get_shortcode_module_template_part('templates/title-with-dots-template', 'title-with-dots', '', $params);

		return $html;

	}

}