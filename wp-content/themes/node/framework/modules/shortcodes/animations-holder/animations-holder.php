<?php
namespace NodeEdge\Modules\Shortcodes\AnimationsHolder;

use NodeEdge\Modules\Shortcodes\Lib\ShortcodeInterface;

class AnimationsHolder implements ShortcodeInterface {
	private $base;

	public function __construct() {
		$this->base = 'edgtf_animations_holder';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(
			array(
				'name'                    => 'Animations Holder',
				'base'                    => $this->base,
				'as_parent'               => array('except' => 'vc_row, vc_accordion, vc_tabs, edgtf_elements_holder, edgtf_pricing_tables, edgtf_text_slider_holder, edgtf_info_card_slider, edgtf_icon_slider'),
				'content_element'         => true,
				'category'                => 'by EDGE',
				'icon'                    => 'icon-wpb-animation-holder extended-custom-icon',
				'show_settings_on_create' => true,
				'js_view'                 => 'VcColumnView',
				'params'                  => array(
					array(
						'type'        => 'dropdown',
						'heading'     => 'Animation',
						'param_name'  => 'css_animation',
						'value'       => array(
							'No animation'                    => '',
							'Elements Shows From Left Side'   => 'edgtf-element-from-left',
							'Elements Shows From Right Side'  => 'edgtf-element-from-right',
							'Elements Shows From Top Side'    => 'edgtf-element-from-top',
							'Elements Shows From Bottom Side' => 'edgtf-element-from-bottom',
							'Elements Shows From Fade'        => 'edgtf-element-from-fade'
						),
						'admin_label' => true,
						'description' => ''
					),
					array(
						'type'        => 'textfield',
						'heading'     => 'Animation Delay (ms)',
						'param_name'  => 'animation_delay',
						'value'       => ''
					),
					array(
						'type'        => 'textfield',
						'heading'     => 'Animation Speed (ms)',
						'param_name'  => 'animation_speed',
						'value'       => ''
					)
				)
			)
		);
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'css_animation'   => '',
			'animation_delay' => '',
			'animation_speed' => '500'
		);

		$params            = shortcode_atts($default_atts, $atts);
		$params['content'] = $content;
		$params['class']   = array(
			'edgtf-animations-holder',
			$params['css_animation']
		);

		$params['style'] = $this->getHolderStyles($params);
		$params['data']  = array(
			'data-animation' => $params['css_animation']
		);

		return node_edge_get_shortcode_module_template_part('templates/animations-holder-template', 'animations-holder', '', $params);
	}

	private function getHolderStyles($params) {
		$styles = array();

		if($params['animation_delay'] !== '') {
			$styles[] = 'transition-delay: '.$params['animation_delay'].'ms';
			$styles[] = '-webkit-animation-delay: '.$params['animation_delay'].'ms';
			$styles[] = 'animation-delay: '.$params['animation_delay'].'ms';
		}

		if($params['animation_speed'] !== '') {
			$styles[] = 'animation-duration: '.$params['animation_speed'].'ms';
			$styles[] = '-webkit-animation-duration: '.$params['animation_speed'].'ms';
			$styles[] = '-moz-animation-duration: '.$params['animation_speed'].'ms';
		}

		return $styles;
	}
}