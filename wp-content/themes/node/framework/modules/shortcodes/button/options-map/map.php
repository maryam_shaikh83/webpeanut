<?php

if(!function_exists('node_edge_button_map')) {
    function node_edge_button_map() {
        $panel = node_edge_add_admin_panel(array(
            'title' => 'Button',
            'name'  => 'panel_button',
            'page'  => '_elements_page'
        ));

        //Typography options
        node_edge_add_admin_section_title(array(
            'name' => 'typography_section_title',
            'title' => 'Typography',
            'parent' => $panel
        ));

        $typography_group = node_edge_add_admin_group(array(
            'name' => 'typography_group',
            'title' => 'Typography',
            'description' => 'Setup typography for all button types',
            'parent' => $panel
        ));

        $typography_row = node_edge_add_admin_row(array(
            'name' => 'typography_row',
            'next' => true,
            'parent' => $typography_group
        ));

        node_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'fontsimple',
            'name'          => 'button_font_family',
            'default_value' => '',
            'label'         => 'Font Family',
        ));

        node_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_text_transform',
            'default_value' => '',
            'label'         => 'Text Transform',
            'options'       => node_edge_get_text_transform_array()
        ));

        node_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_font_style',
            'default_value' => '',
            'label'         => 'Font Style',
            'options'       => node_edge_get_font_style_array()
        ));

        node_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'textsimple',
            'name'          => 'button_letter_spacing',
            'default_value' => '',
            'label'         => 'Letter Spacing',
            'args'          => array(
                'suffix' => 'px'
            )
        ));

        $typography_row2 = node_edge_add_admin_row(array(
            'name' => 'typography_row2',
            'next' => true,
            'parent' => $typography_group
        ));

        node_edge_add_admin_field(array(
            'parent'        => $typography_row2,
            'type'          => 'selectsimple',
            'name'          => 'button_font_weight',
            'default_value' => '',
            'label'         => 'Font Weight',
            'options'       => node_edge_get_font_weight_array()
        ));

        //Outline type options
        node_edge_add_admin_section_title(array(
            'name' => 'type_section_title',
            'title' => 'Types',
            'parent' => $panel
        ));

        $outline_group = node_edge_add_admin_group(array(
            'name' => 'outline_group',
            'title' => 'Outline Type',
            'description' => 'Setup outline button type',
            'parent' => $panel
        ));

        $outline_row = node_edge_add_admin_row(array(
            'name' => 'outline_row',
            'next' => true,
            'parent' => $outline_group
        ));

        node_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_text_color',
            'default_value' => '',
            'label'         => 'Text Color',
            'description'   => ''
        ));

        node_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_text_color',
            'default_value' => '',
            'label'         => 'Text Hover Color',
            'description'   => ''
        ));

        node_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_bg_color',
            'default_value' => '',
            'label'         => 'Hover Background Color',
            'description'   => ''
        ));

        node_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_border_color',
            'default_value' => '',
            'label'         => 'Border Color',
            'description'   => ''
        ));

        $outline_row2 = node_edge_add_admin_row(array(
            'name' => 'outline_row2',
            'next' => true,
            'parent' => $outline_group
        ));

        node_edge_add_admin_field(array(
            'parent'        => $outline_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_border_color',
            'default_value' => '',
            'label'         => 'Hover Border Color',
            'description'   => ''
        ));

	    //Outline Light type options
	    $outline_light_group = node_edge_add_admin_group(array(
		    'name' => 'outline_light_group',
		    'title' => 'Outline Light Type',
		    'description' => 'Setup outline light button type',
		    'parent' => $panel
	    ));

	    $outline_light_row = node_edge_add_admin_row(array(
		    'name' => 'outline_light_row',
		    'next' => true,
		    'parent' => $outline_light_group
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $outline_light_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_text_color',
		    'default_value' => '',
		    'label'         => 'Text Color',
		    'description'   => ''
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $outline_light_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_hover_text_color',
		    'default_value' => '',
		    'label'         => 'Text Hover Color',
		    'description'   => ''
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $outline_light_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_hover_bg_color',
		    'default_value' => '',
		    'label'         => 'Hover Background Color',
		    'description'   => ''
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $outline_light_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_border_color',
		    'default_value' => '',
		    'label'         => 'Border Color',
		    'description'   => ''
	    ));

	    $outline_light_row2 = node_edge_add_admin_row(array(
		    'name' => 'outline_light_row2',
		    'next' => true,
		    'parent' => $outline_light_group
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $outline_light_row2,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_outline_light_hover_border_color',
		    'default_value' => '',
		    'label'         => 'Hover Border Color',
		    'description'   => ''
	    ));

        //Solid type options
        $solid_group = node_edge_add_admin_group(array(
            'name' => 'solid_group',
            'title' => 'Solid Type',
            'description' => 'Setup solid button type',
            'parent' => $panel
        ));

        $solid_row = node_edge_add_admin_row(array(
            'name' => 'solid_row',
            'next' => true,
            'parent' => $solid_group
        ));

        node_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_text_color',
            'default_value' => '',
            'label'         => 'Text Color',
            'description'   => ''
        ));

        node_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_text_color',
            'default_value' => '',
            'label'         => 'Text Hover Color',
            'description'   => ''
        ));

        node_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_bg_color',
            'default_value' => '',
            'label'         => 'Background Color',
            'description'   => ''
        ));

        node_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_bg_color',
            'default_value' => '',
            'label'         => 'Hover Background Color',
            'description'   => ''
        ));

        $solid_row2 = node_edge_add_admin_row(array(
            'name' => 'solid_row2',
            'next' => true,
            'parent' => $solid_group
        ));

        node_edge_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_border_color',
            'default_value' => '',
            'label'         => 'Border Color',
            'description'   => ''
        ));

        node_edge_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_border_color',
            'default_value' => '',
            'label'         => 'Hover Border Color',
            'description'   => ''
        ));

	    //Solid dark type options
	    $solid_dark_group = node_edge_add_admin_group(array(
		    'name' => 'solid_dark_group',
		    'title' => 'Solid Dark Type',
		    'description' => 'Setup Solid Dark button type',
		    'parent' => $panel
	    ));

	    $solid_dark_row = node_edge_add_admin_row(array(
		    'name' => 'solid_dark_row',
		    'next' => true,
		    'parent' => $solid_dark_group
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_text_color',
		    'default_value' => '',
		    'label'         => 'Text Color',
		    'description'   => ''
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_hover_text_color',
		    'default_value' => '',
		    'label'         => 'Text Hover Color',
		    'description'   => ''
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_bg_color',
		    'default_value' => '',
		    'label'         => 'Background Color',
		    'description'   => ''
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_hover_bg_color',
		    'default_value' => '',
		    'label'         => 'Hover Background Color',
		    'description'   => ''
	    ));

	    $solid_dark_row2 = node_edge_add_admin_row(array(
		    'name' => 'solid_dark_row2',
		    'next' => true,
		    'parent' => $solid_dark_group
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row2,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_border_color',
		    'default_value' => '',
		    'label'         => 'Border Color',
		    'description'   => ''
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $solid_dark_row2,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_solid_dark_hover_border_color',
		    'default_value' => '',
		    'label'         => 'Hover Border Color',
		    'description'   => ''
	    ));

	    //Transparent type options
	    $transparent_group = node_edge_add_admin_group(array(
		    'name' => 'transparent_group',
		    'title' => 'Transparent Type',
		    'description' => 'Setup Transparent button type',
		    'parent' => $panel
	    ));

	    $transparent_row = node_edge_add_admin_row(array(
		    'name' => 'transparent_row',
		    'next' => true,
		    'parent' => $transparent_group
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $transparent_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_transparent_text_color',
		    'default_value' => '',
		    'label'         => 'Text Color',
		    'description'   => ''
	    ));

	    node_edge_add_admin_field(array(
		    'parent'        => $transparent_row,
		    'type'          => 'colorsimple',
		    'name'          => 'btn_transparent_hover_text_color',
		    'default_value' => '',
		    'label'         => 'Text Hover Color',
		    'description'   => ''
	    ));
    }

    add_action('node_edge_options_elements_map', 'node_edge_button_map');
}

