<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>" <?php node_edge_inline_style($button_styles); ?> <?php node_edge_class_attribute($button_classes); ?> <?php echo node_edge_get_inline_attrs($button_data); ?> <?php echo node_edge_get_inline_attrs($button_custom_attrs); ?>>
	<span class="edgtf-btn-text"><?php echo esc_html($text); ?></span>
    <?php echo node_edge_icon_collections()->renderIcon($icon, $icon_pack); ?>
</a>