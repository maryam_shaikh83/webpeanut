<?php
namespace NodeEdge\Modules\Shortcodes\Lib;

use NodeEdge\Modules\ImageWithText\ImageWithText;
use NodeEdge\Modules\Shortcodes\Accordion\Accordion;
use NodeEdge\Modules\Shortcodes\AccordionTab\AccordionTab;
use NodeEdge\Modules\Shortcodes\AnimationsHolder\AnimationsHolder;
use NodeEdge\Modules\Shortcodes\Blockquote\Blockquote;
use NodeEdge\Modules\Shortcodes\BlogList\BlogList;
use NodeEdge\Modules\Shortcodes\BlogSlider\BlogSlider;
use NodeEdge\Modules\Shortcodes\Button\Button;
use NodeEdge\Modules\Shortcodes\CallToAction\CallToAction;
use NodeEdge\Modules\Shortcodes\Counter\Countdown;
use NodeEdge\Modules\Shortcodes\Counter\Counter;
use NodeEdge\Modules\Shortcodes\CustomFont\CustomFont;
use NodeEdge\Modules\Shortcodes\Dropcaps\Dropcaps;
use NodeEdge\Modules\Shortcodes\ElementsHolder\ElementsHolder;
use NodeEdge\Modules\Shortcodes\ElementsHolderItem\ElementsHolderItem;
use NodeEdge\Modules\Shortcodes\GoogleMap\GoogleMap;
use NodeEdge\Modules\Shortcodes\Highlight\Highlight;
use NodeEdge\Modules\Shortcodes\Icon\Icon;
use NodeEdge\Modules\Shortcodes\IconListItem\IconListItem;
use NodeEdge\Modules\Shortcodes\IconWithText\IconWithText;
use NodeEdge\Modules\Shortcodes\ImageGallery\ImageGallery;
use NodeEdge\Modules\Shortcodes\Message\Message;
use NodeEdge\Modules\Shortcodes\OrderedList\OrderedList;
use NodeEdge\Modules\Shortcodes\PieCharts\PieChartBasic\PieChartBasic;
use NodeEdge\Modules\Shortcodes\PieCharts\PieChartDoughnut\PieChartDoughnut;
use NodeEdge\Modules\Shortcodes\PieCharts\PieChartDoughnut\PieChartPie;
use NodeEdge\Modules\Shortcodes\PieCharts\PieChartWithIcon\PieChartWithIcon;
use NodeEdge\Modules\Shortcodes\PricingTables\PricingTables;
use NodeEdge\Modules\Shortcodes\PricingTable\PricingTable;
use NodeEdge\Modules\Shortcodes\Process\ProcessHolder;
use NodeEdge\Modules\Shortcodes\Process\ProcessItem;
use NodeEdge\Modules\Shortcodes\ProgressBar\ProgressBar;
use NodeEdge\Modules\Shortcodes\Separator\Separator;
use NodeEdge\Modules\Shortcodes\SocialShare\SocialShare;
use NodeEdge\Modules\Shortcodes\Tabs\Tabs;
use NodeEdge\Modules\Shortcodes\Tab\Tab;
use NodeEdge\Modules\Shortcodes\Team\Team;
use NodeEdge\Modules\Shortcodes\TitleWithDots\TitleWithDots;
use NodeEdge\Modules\Shortcodes\TitleWithNumber\TitleWithNumber;
use NodeEdge\Modules\Shortcodes\UnorderedList\UnorderedList;
use NodeEdge\Modules\Shortcodes\VideoButton\VideoButton;
use NodeEdge\Modules\Shortcodes\ShopMasonry\ShopMasonry;
use NodeEdge\Modules\Shortcodes\ItemShowcase\ItemShowcase;
use NodeEdge\Modules\Shortcodes\ItemShowcaseListItem\ItemShowcaseListItem;

/**
 * Class ShortcodeLoader
 */
class ShortcodeLoader {
	/**
	 * @var private instance of current class
	 */
	private static $instance;
	/**
	 * @var array
	 */
	private $loadedShortcodes = array();

	/**
	 * Private constuct because of Singletone
	 */
	private function __construct() {}

	/**
	 * Private sleep because of Singletone
	 */
	private function __wakeup() {}

	/**
	 * Private clone because of Singletone
	 */
	private function __clone() {}

	/**
	 * Returns current instance of class
	 * @return ShortcodeLoader
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			return new self;
		}

		return self::$instance;
	}

	/**
	 * Adds new shortcode. Object that it takes must implement ShortcodeInterface
	 * @param ShortcodeInterface $shortcode
	 */
	private function addShortcode(ShortcodeInterface $shortcode) {
		if(!array_key_exists($shortcode->getBase(), $this->loadedShortcodes)) {
			$this->loadedShortcodes[$shortcode->getBase()] = $shortcode;
		}
	}

	/**
	 * Adds all shortcodes.
	 *
	 * @see ShortcodeLoader::addShortcode()
	 */
	private function addShortcodes() {
		$this->addShortcode(new Accordion());
		$this->addShortcode(new AccordionTab());
		$this->addShortcode(new AnimationsHolder());
		$this->addShortcode(new Blockquote());
		$this->addShortcode(new BlogList());
		$this->addShortcode(new BlogSlider());
		$this->addShortcode(new Button());
		$this->addShortcode(new CallToAction());
		$this->addShortcode(new Counter());
		$this->addShortcode(new Countdown());
		$this->addShortcode(new CustomFont());
		$this->addShortcode(new Dropcaps());
		$this->addShortcode(new ElementsHolder());
		$this->addShortcode(new ElementsHolderItem());
		$this->addShortcode(new GoogleMap());
		$this->addShortcode(new Highlight());
		$this->addShortcode(new Icon());
		$this->addShortcode(new IconListItem());
		$this->addShortcode(new IconWithText());
		$this->addShortcode(new ImageGallery());
		$this->addShortcode(new ImageWithText());
		$this->addShortcode(new ItemShowcase());
		$this->addShortcode(new ItemShowcaseListItem());
		$this->addShortcode(new Message());
		$this->addShortcode(new OrderedList());
		$this->addShortcode(new PieChartBasic());
		$this->addShortcode(new PieChartPie());
		$this->addShortcode(new PieChartDoughnut());
		$this->addShortcode(new PieChartWithIcon());
		$this->addShortcode(new PricingTables());
		$this->addShortcode(new PricingTable());
		$this->addShortcode(new ProgressBar());
		$this->addShortcode(new ProcessHolder());
		$this->addShortcode(new ProcessItem());
		$this->addShortcode(new Separator());
		$this->addShortcode(new SocialShare());
		$this->addShortcode(new Tabs());
		$this->addShortcode(new Tab());
		$this->addShortcode(new Team());
		$this->addShortcode(new TitleWithDots());
		$this->addShortcode(new TitleWithNumber());
		$this->addShortcode(new UnorderedList());
		$this->addShortcode(new VideoButton());
		$this->addShortcode(new ShopMasonry());
	}
	/**
	 * Calls ShortcodeLoader::addShortcodes and than loops through added shortcodes and calls render method
	 * of each shortcode object
	 */
	public function load() {
		$this->addShortcodes();

		foreach ($this->loadedShortcodes as $shortcode) {
			add_shortcode($shortcode->getBase(), array($shortcode, 'render'));
		}
	}
}

$shortcodeLoader = ShortcodeLoader::getInstance();
$shortcodeLoader->load();