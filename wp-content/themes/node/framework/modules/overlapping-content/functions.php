<?php

if(!function_exists('node_edge_overlapping_content_enabled')) {
    /**
     * Checks if overlapping content is enabled
     *
     * @return bool
     */
    function node_edge_overlapping_content_enabled() {
        $id = node_edge_get_page_id();

        return get_post_meta($id, 'edgtf_overlapping_content_enable_meta', true) === 'yes';
    }
}

if(!function_exists('node_edge_overlapping_content_class')) {
    /**
     * Adds overlapping content class to body tag
     * if overlapping content is enabled
     * @param $classes
     *
     * @return array
     */
    function node_edge_overlapping_content_class($classes) {
        if(node_edge_overlapping_content_enabled()) {
            $classes[] = 'edgtf-overlapping-content-enabled';
        }

        return $classes;
    }

    add_filter('body_class', 'node_edge_overlapping_content_class');
}

if(!function_exists('node_edge_overlapping_content_amount')) {
    /**
     * Returns amount of overlapping content
     *
     * @return int
     */
    function node_edge_overlapping_content_amount() {
        return 75;
    }
}

if(!function_exists('node_edge_oc_content_top_padding')) {
    function node_edge_oc_content_top_padding($style) {
	    $id = node_edge_get_page_id();

	    $class_prefix = node_edge_get_unique_page_class();

	    $content_selector = array(
		    $class_prefix.' .edgtf-content .edgtf-content-inner > .edgtf-container .edgtf-overlapping-content'
	    );

	    $content_class = array();

	    $page_padding_top = get_post_meta($id, 'edgtf_page_content_top_padding', true);
		$page_padding_right = get_post_meta($id, "edgtf_page_content_right_padding", true);
		$page_padding_bottom = get_post_meta($id, "edgtf_page_content_bottom_padding", true);
		$page_padding_left = get_post_meta($id, "edgtf_page_content_left_padding", true);

	    if($page_padding_top !== '') {
		    if(get_post_meta($id, 'edgtf_page_content_top_padding_mobile', true) == 'yes') {
			    $content_class['padding-top'] = node_edge_filter_px($page_padding_top).'px!important';
		    } else {
			    $content_class['padding-top'] = node_edge_filter_px($page_padding_top).'px';
		    }

	    }

		if($page_padding_bottom !== '') {
			$content_class['padding-bottom'] = node_edge_filter_px($page_padding_bottom).'px';
		}
		if($page_padding_left !== '') {
			$content_class['padding-left'] = node_edge_filter_px($page_padding_left).'px';
		}
		if($page_padding_right !== '') {
			$content_class['padding-right'] = node_edge_filter_px($page_padding_right).'px';
		}

		if(!empty ($content_class)) {
			$style[] = node_edge_dynamic_css($content_selector, $content_class);
		}

	    return $style;
    }

	add_filter('node_edge_add_page_custom_style', 'node_edge_oc_content_top_padding');
}