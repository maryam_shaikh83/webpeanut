<?php
	if(!function_exists('node_edge_layerslider_overrides')) {
		/**
		 * Disables Layer Slider auto update box
		 */
		function node_edge_layerslider_overrides() {
			$GLOBALS['lsAutoUpdateBox'] = false;
		}

		add_action('layerslider_ready', 'node_edge_layerslider_overrides');
	}
?>