<?php if(node_edge_options()->getOptionValue('enable_social_share') == 'yes'
    && node_edge_options()->getOptionValue('enable_social_share_on_portfolio-item') == 'yes') : ?>
    <div class="edgtf-portfolio-social">
        <?php echo node_edge_get_social_share_html() ?>
    </div>
<?php endif; ?>