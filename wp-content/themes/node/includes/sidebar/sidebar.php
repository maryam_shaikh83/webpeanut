<?php

if(!function_exists('node_edge_register_sidebars')) {
    /**
     * Function that registers theme's sidebars
     */
    function node_edge_register_sidebars() {

        register_sidebar(array(
            'name' => 'Sidebar',
            'id' => 'sidebar',
            'description' => 'Default Sidebar',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="edgtf-widget-title">',
            'after_title' => '</h4>' . node_edge_get_separator_html(array('position' => 'left', 'class_name' => 'edgtf-sidebar-title-separator'))
        ));

    }

    add_action('widgets_init', 'node_edge_register_sidebars');
}

if(!function_exists('node_edge_add_support_custom_sidebar')) {
    /**
     * Function that adds theme support for custom sidebars. It also creates NodeEdgeSidebar object
     */
    function node_edge_add_support_custom_sidebar() {
        add_theme_support('NodeEdgeSidebar');
        if (get_theme_support('NodeEdgeSidebar')) new NodeEdgeSidebar();
    }

    add_action('after_setup_theme', 'node_edge_add_support_custom_sidebar');
}
