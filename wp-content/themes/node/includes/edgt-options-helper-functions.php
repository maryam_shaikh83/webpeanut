<?php

if(!function_exists('node_edge_is_responsive_on')) {
    /**
     * Checks whether responsive mode is enabled in theme options
     * @return bool
     */
    function node_edge_is_responsive_on() {
        return node_edge_options()->getOptionValue('responsiveness') !== 'no';
    }
}