<?php if($query_results->max_num_pages>1){ ?>
	<div class="edgtf-ptf-list-paging">
		<span class="edgtf-ptf-list-load-more">
			<a href="#"><?php esc_html_e('Load more','edgt_core'); ?></a>
		</span>
		<div class="edgtf-nodes">
			<div class="edgtf-node-1"></div>
			<div class="edgtf-node-2"></div>
			<div class="edgtf-node-3"></div>
			<div class="edgtf-node-4"></div>
			<div class="edgtf-node-5"></div>
			<div class="edgtf-node-6"></div>
			<div class="edgtf-node-7"></div>
			<div class="edgtf-node-8"></div>
			<div class="edgtf-node-9"></div>
			<div class="edgtf-node-10"></div>
			<div class="edgtf-node-11"></div>
			<div class="edgtf-node-12"></div>
			<div class="edgtf-node-13"></div>
			<div class="edgtf-node-14"></div>
			<div class="edgtf-node-15"></div>
			<div class="edgtf-node-16"></div>
		</div>
	</div>
<?php }